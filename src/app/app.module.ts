import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { IntroComponent } from './modules/intro/intro.component';
import { AppRoutingModule } from './app.routing';
@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    IntroComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }