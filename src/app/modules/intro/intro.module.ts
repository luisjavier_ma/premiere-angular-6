import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IntroComponent } from './intro.component';
@NgModule({
  imports: [
    BrowserModule
  ],
  declarations: [IntroComponent],
  exports: [IntroComponent]
})
export class IntroModule {}